﻿using System;
using System.Threading;

namespace Task1._3_Thread
{
    public struct Arguments
    {
        public int parametr1, parametr2, parametr3, parametr4, parametr5;
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Task 1.3: Create and run a thread that executes an anonymous method, and pass in it a lot of arguments of the structural type.");
            Console.WriteLine(new string('-', 70));

            Arguments arguments = new Arguments();
            arguments.parametr1 = 1;
            arguments.parametr2 = 2;
            arguments.parametr3 = 3;
            arguments.parametr4 = 4;
            arguments.parametr5 = 5;

            ParameterizedThreadStart methodWithStructParameters = new ParameterizedThreadStart((object obj) =>
                                                            {
                                                                Arguments argument = (Arguments)obj;
                                                                Console.Write("parametr1:{0}, parametr2:{1}, parametr3:{2}, parametr4:{3}, parametr5:{4}",
                                                                argument.parametr1, argument.parametr2, argument.parametr3, argument.parametr4, argument.parametr5);
                                                            });
            Thread thread = new Thread(methodWithStructParameters);
            thread.Start((object)arguments);

            Console.ReadKey();
        }
    }
}
