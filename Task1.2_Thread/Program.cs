﻿using System;
using System.Threading;

namespace Task1._2_Thread
{
    class Program
    {
        static void MethodWithOneParameter(object parametr)
        {
            Console.WriteLine(new string(' ', 140) + "Method with one object type parameter.");
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Task 1.2: Create and run the thread that the method executes, with one parameter of type object.");
            Console.WriteLine(new string('-', 60));

            Program objectParametr = new Program() { };
            ParameterizedThreadStart methodWithOneParameter = new ParameterizedThreadStart(MethodWithOneParameter);
            Thread thread = new Thread(methodWithOneParameter);
            thread.Start(objectParametr);

            Console.WriteLine("The main thread.");

            Console.ReadKey();
        }
    }
}
