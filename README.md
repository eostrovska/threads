### What is this repository for? ###

* Task 1.1: Create and run the thread that the method executes, without parameters.
* Task 1.2: Create and run the thread that the method executes, with one parameter of type object.
* Task 1.3: Create and run a thread that executes an anonymous method, and pass in it a lot of arguments of the structural type.
* Task 1.4: In one of the methods add a delay of Thread.Sleep. And make it possible so that the mail thread waits until this method finishes its work.
* Create a program that will display the chain of falling symbols.The first character of the chain is white,
  the second character is light-green, the other symbols are dark green.Reaching the end, the chain disappears
  and a new chain is formed on top.

