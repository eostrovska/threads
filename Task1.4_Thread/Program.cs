﻿using System;
using System.Threading;

namespace Task1._4_Thread
{
    class Program
    {
        static void MethodWithoutParameters()
        {
            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine(new string(' ', 150) + "Method without parameters.");
                Thread.Sleep(200);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Task 1.4: In one of the methods add a delay of Thread.Sleep. And make it possible so that the mail thread waits until this method finishes its work.");
            Console.WriteLine(new string('-', 70));

            Thread thread = new Thread(MethodWithoutParameters);
            thread.Start();

            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine("The main thread.");
                Thread.Sleep(100);
            }

            Console.ReadKey();
        }
    }
}
