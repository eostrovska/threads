﻿using System.Drawing;
using System.Windows.Forms;

namespace WindowsFormsAbstractFactory
{
    public partial class Form1 : Form
    {
        AbstractWidgetFactory widgetFactory = null;
        UserControlClient userControlClient = null;

        public Form1()
        {
            //this.widgetFactory = new AppleWidgetFactory();
            //this.widgetFactory = new WindowsWidgetFactory();
            this.widgetFactory = new AndroidWidgetFactory();
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.userControlClient = new UserControlClient(widgetFactory);
            this.ClientSize = new Size(400, 200);
            this.Controls.Add(this.userControlClient);
            this.Text = "Abstract Factory";
        }
    }
}
