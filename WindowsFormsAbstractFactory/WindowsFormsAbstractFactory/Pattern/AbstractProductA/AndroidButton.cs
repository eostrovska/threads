﻿using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    class AndroidButton : AbstractButton
    {
        public AndroidButton()
            : base()
        {
            this.Text = "Android";
            this.ForeColor = Color.LightBlue;
            this.BackColor = Color.Purple;
        }
    }
}
