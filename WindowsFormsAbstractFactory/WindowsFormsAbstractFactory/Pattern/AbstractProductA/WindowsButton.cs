﻿using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    // ConcreteProductB
    class WindowsButton : AbstractButton
    {
        public WindowsButton()
            : base()
        {
            this.Text = "Windows";
            this.ForeColor = Color.Aqua;
            this.BackColor = Color.DarkBlue;
        }
    }
}
