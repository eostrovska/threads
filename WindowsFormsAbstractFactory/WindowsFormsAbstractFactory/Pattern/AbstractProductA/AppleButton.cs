﻿using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    class AppleButton : AbstractButton
    {
        public AppleButton()
            : base()
        {
            this.Text = "Apple";
            this.ForeColor = Color.White;
            this.BackColor = Color.LightGray;
        }
    }
}
