﻿using System.Windows.Forms;
using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    class AbstractButton : Button
    {
        public AbstractButton()
            : base()
        {
            this.Location = new Point(14, 47);
            this.Size = new Size(124, 23);
        }
    }
}
