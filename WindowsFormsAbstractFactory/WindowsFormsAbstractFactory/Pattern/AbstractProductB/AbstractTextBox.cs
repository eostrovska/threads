﻿using System.Windows.Forms;
using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    // AbstractProductB
    class AbstractTextBox : TextBox
    {
        public AbstractTextBox()
            : base()
        {
            this.Location = new Point(14, 15);
            this.Size = new Size(124, 20);
            this.Text = "Введите текст";
        }
    }
}
