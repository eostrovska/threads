﻿using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    class AppleTextBox : AbstractTextBox
    {
        public AppleTextBox()
            : base()
        {
            this.ForeColor = Color.DarkGray;
        }
    }
}
