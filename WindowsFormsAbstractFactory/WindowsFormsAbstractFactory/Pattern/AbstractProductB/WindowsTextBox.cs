﻿using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    class WindowsTextBox : AbstractTextBox
    {
        public WindowsTextBox()
            : base()
        {
            this.ForeColor = Color.DarkBlue;
        }
    }
}
