﻿using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    class AndroidTextBox : AbstractTextBox
    {
        public AndroidTextBox()
            : base()
        {
            this.ForeColor = Color.Black;
        }
    }
}
