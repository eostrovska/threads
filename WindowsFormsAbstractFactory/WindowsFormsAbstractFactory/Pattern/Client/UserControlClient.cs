﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    class UserControlClient : UserControl
    {        
        AbstractButton button = null;
        AbstractTextBox textBox = null;

        public UserControlClient(AbstractWidgetFactory widgetFactory)
        {
            button = widgetFactory.CreateButton();
            textBox = widgetFactory.CreateTextBox();

            InitializeComponent();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this.textBox.Text);
        }

        private void InitializeComponent()
        {                     
            this.button.Click += new EventHandler(this.button1_Click);
                                      
            this.Location = new Point(130, 50);
            this.Size = new Size(150, 100);
            this.BackColor = button.BackColor;
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.button);                      
        } 
    }
}
