﻿using System.Windows.Forms;
using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    class WindowsWidgetFactory : AbstractWidgetFactory
    {
        public override AbstractButton CreateButton()
        { 
            return new WindowsButton();
        }

        public override AbstractTextBox CreateTextBox()
        {
            return new WindowsTextBox();
        }
    }
}
