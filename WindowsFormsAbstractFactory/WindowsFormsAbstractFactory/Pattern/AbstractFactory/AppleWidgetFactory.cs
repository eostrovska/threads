﻿using System.Windows.Forms;
using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    class AppleWidgetFactory : AbstractWidgetFactory
    {
        public override AbstractButton CreateButton()
        {
            return new AppleButton();
        }

        public override AbstractTextBox CreateTextBox()
        {
            return new AppleTextBox();
        }
    }
}
