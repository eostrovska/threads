﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsAbstractFactory
{
    class LinuxWidgetFactory:AbstractWidgetFactory
    {
        public override AbstractButton CreateButton()
        {
            return new LinuxButton();
        }

        public override AbstractTextBox CreateTextBox()
        {
            return new LinuxTextBox();
        }
    }
}
