﻿using System.Windows.Forms;
using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    abstract class AbstractWidgetFactory
    {
        public abstract AbstractButton CreateButton();
        public abstract AbstractTextBox CreateTextBox();
    }
}
