﻿using System.Windows.Forms;
using System.Drawing;

namespace WindowsFormsAbstractFactory
{
    class AndroidWidgetFactory : AbstractWidgetFactory
    {
        public override AbstractButton CreateButton()
        {
            return new AndroidButton();
        }

        public override AbstractTextBox CreateTextBox()
        {
            return new AndroidTextBox();
        }
    }
}
