﻿using System;
using System.Threading;

namespace Task1._1_Thread
{
    class Program
    {
        static void MethodWithoutParameters()
        {
            Console.WriteLine(new string(' ', 140) + "Method without parameters.");
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Task 1.1: Create and run the thread that the method executes, without parameters.");
            Console.WriteLine(new string('-', 60));

            Thread thread = new Thread(MethodWithoutParameters);
            thread.Start();

            Console.WriteLine("The main thread.");

            Console.ReadKey();
        }
    }
}
