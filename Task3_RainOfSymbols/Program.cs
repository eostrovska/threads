﻿using System;
using System.Threading;

namespace Task3_RainOfSymbols
{
    class Program
    {
        static void Main()
        {
            //Console.WriteLine("Create a program that will display the chain of falling symbols.The first character of the chain is white," + 
            //                  "the second character is light-green, the other symbols are dark green.Reaching the end, the chain disappears" + 
            //                  "and a new chain is formed on top.");
            Console.SetWindowSize(80, 42);
            Matrix instance;

            for (int i = 0; i < 26; i++)
            {
                instance = new Matrix(i * 3, true);
                new Thread(instance.Move).Start();
            }
        }
    }

    
}